Willkommen im offiziellen GitLab-Repository von XFoerderleistungsbeschreibung (XFLB)! Dieses Wiki dient als zentrale Anlaufstelle für alle Informationen rund um den XFLB-Standard.

Der XFLB-Standard zielt darauf ab, die Beschreibung und den Austausch von Förderleistungsdaten in der öffentlichen Verwaltung zu standardisieren und zu harmonisieren. Ziel des Standards ist es, die digitale Transformation im deutschen Förderwesen voranzutreiben und zu einem effizienteren, transparenteren und zugänglicheren Prozess beizutragen. Mit der Konsolidierung des Standards über alle Bundesländer hinweg verfolgen wir das Ziel, eine flexible und interoperable Nutzung durch Bund und Länder zu ermöglichen, um so einen synchronen Datenbestand und damit eine Datenkonsistenz in Bundes-, Landes- und Fachportalen zu gewährleisten.

## **Bewertung des XFLB und Mitwirkung**

Auf der Seite [Aktuelle Version & Spezifikationen](https://gitlab.opencode.de/OC000029002073/xflb/-/wikis/Startseite/3.-Dokumentation-&-Ressourcen/Aktuelle-Version-&-Spezifikationen) finden Sie sämtliche Spezifikationsunterlagen zum XFLB. Für eine umfassende Bewertung des XFLB und zur Mitwirkung empfehlen wir, den Bereich [3. Dokumentation & Ressourcen](https://gitlab.opencode.de/OC000029002073/xflb/-/wikis/Startseite/3.-Dokumentation-&-Ressourcen) im Wiki zu erkunden.

Bitte richten Sie Ihr Feedback zu XFoerderleistungsbeschreibung an die E-Mail-Adresse: xflb@stmd.bayern.de oder nutzen Sie die [Issues-Funktion](https://gitlab.opencode.de/OC000029002073/xflb/-/issues) des Gitlabs. Für letztere benötigen Sie einen GitLab-Account.

Weitere Informationen finden Sie unter [4. Mitwirkung & Feedback](https://gitlab.opencode.de/OC000029002073/xflb/-/wikis/Startseite/4.-Mitwirkung-&-Feedback).

Für sämtliche weitere Hintergrundinformationen navigieren Sie sich bitte durch das GitLab Wiki.

## **Hinweise zur Navigation im Wiki**

**Navigieren Sie sich über die Seitenleiste rechts durch das Wiki und Repository, um mehr über den XFLB-Standard zu erfahren, wie Sie beitragen können, und um Zugang zu wichtigen Ressourcen und Dokumentationen zu erhalten.**

## Inhaltsverzeichnis

* [Startseite](https://gitlab.opencode.de/OC000029002073/xflb/-/wikis/0.-Startseite)
  * [1. Über XFLB](https://gitlab.opencode.de/OC000029002073/xflb/-/wikis/Startseite/1.-%C3%9Cber-XFLB)
  * [2. Aktuelles & Updates](https://gitlab.opencode.de/OC000029002073/xflb/-/wikis/Startseite/2.-Aktuelles-&-Updates)
  * [3. Dokumentation & Ressourcen](https://gitlab.opencode.de/OC000029002073/xflb/-/wikis/Startseite/3.-Dokumentation-&-Ressourcen)
    * [Aktuelle Version & Spezifikationen](https://gitlab.opencode.de/OC000029002073/xflb/-/wikis/Startseite/3.-Dokumentation-&-Ressourcen/Aktuelle-Version-&-Spezifikationen)
    * [Datenfelder und Relationen](https://gitlab.opencode.de/OC000029002073/xflb/-/wikis/Startseite/3.-Dokumentation-&-Ressourcen/Datenfelder-und-Relationen)
    * [Implementierung & Referenzanwendung](https://gitlab.opencode.de/OC000029002073/xflb/-/wikis/Startseite/3.-Dokumentation-&-Ressourcen/Implementierung-&-Referenzanwendung-(XFLB-Validator))
    * [Stakeholder Feedback](https://gitlab.opencode.de/OC000029002073/xflb/-/wikis/0.-Startseite/3.-Dokumentation-&-Ressourcen/Stakeholder-Feedback)
  * [4. Mitwirkung & Feedback](https://gitlab.opencode.de/OC000029002073/xflb/-/wikis/Startseite/4.-Mitwirkung-&-Feedback)
  * [5. Partnerprojekt Förderfinder](https://gitlab.opencode.de/OC000029002073/xflb/-/wikis/Startseite/5.-Partnerprojekt-F%C3%B6rderfinder)
  * [6. FAQ & Kontakt](https://gitlab.opencode.de/OC000029002073/xflb/-/wikis/Startseite/7.-FAQ-&-Kontakt)
  * [Lizenz](https://gitlab.opencode.de/OC000029002073/xflb/-/wikis/Startseite/Lizenz)

## Über das Projekt

Der XFLB-Standard ist ein entscheidender Schritt hin zu transparenteren, effizienteren und zugänglicheren Förderprozessen. Durch die Bereitstellung einer einheitlichen Struktur für Förderleistungsdaten ermöglicht der Standard eine bessere Zusammenarbeit zwischen Bund und Ländern und fördert die Digitalisierung der Verwaltung. Der XFLB-Standard entstand aus den Bestrebungen des OZG-Themenfelds "Forschung und Förderung" und zielt darauf ab, einheitliche Strukturen für die Beschreibung von Förderleistungen zu schaffen. Wir befinden uns aktuell in der Phase der Konsolidierung und Weiterentwicklung dieses Standards und laden alle interessierten Stakeholder zur aktiven Teilnahme ein.

Der Bedarfsträger ist das Land Bayern. Verantwortlich für die Umsetzung und den Betrieb ist das **Bayerische Staatsministerium für Digitales** in Zusammenarbeit mit **mgm technology partners GmbH**.

### Ziele des XFLB-Standards

- **Deutschlandweite Vereinheitlichung und Harmonisierung** von Förderleistungen.
- **Förderung der digitalen Transformation** im Förderwesen.
- **Steigerung der Transparenz und Effizienz** der Förderprozesse.
- **Erleichterung der Zusammenarbeit** zwischen allen beteiligten Stakeholdern.

Erfahren Sie hier mehr über die Hintergrund und Ziele des Standards.

## Wie Sie beitragen können

Ihr Feedback und Ihre Expertise sind entscheidend für die erfolgreiche Weiterentwicklung des XFLB-Standards. Hier sind einige Wege, wie Sie sich einbringen können:

### Feedback geben

Wir laden alle Stakeholder ein, Feedback, Verbesserungsvorschläge und Bedenken hinsichtlich des XFLB-Standards zu teilen. Bitte richten Sie Ihr Feedback an xflb@stmd.bayern.de oder nutzen Sie die [Issues-Funktion](https://gitlab.opencode.de/OC000029002073/xflb/-/issues) dieses Repositories, um Ihre Gedanken und Vorschläge einzubringen.

Erfahren Sie mehr unter [Mitwirkung & Feedback](https://gitlab.opencode.de/OC000029002073/xflb/-/wikis/Startseite/4.-Mitwirkung-&-Feedback).

## Dokumentation und Ressourcen

Informieren Sie sich [hier](https://gitlab.opencode.de/OC000029002073/xflb/-/wikis/Startseite/3.-Dokumentation-&-Ressourcen) über die Entwicklung, den aktuellen Stand und geplante Features.

- **Spezifikationen des XFLB-Standards**: Einsehbar hier im [Repository Dokumentation und Ressourcen](https://gitlab.opencode.de/OC000029002073/xflb/-/tree/master/docs/Dokumentation%20und%20Ressourcen?ref_type=heads) oder auf der [XRepository-Webseite](https://www.xrepository.de/details/urn:xoev-de:kosit:standard:xflb).
- **Detailansicht des XFLB**: Abrufbar unter unserem [Conceptboard-Link](https://app.conceptboard.eu/board/ru77-gsxe-b6t2-trhg-ry1t).
- **Excel-Datenmodell**: Zur Bewertung können Sie die [Excel-Datei](https://gitlab.opencode.de/OC000029002073/xflb/-/blob/06f4e3f70662e1d59c72d8b93e4b7df8a25e87ec/docs/Dokumentation%20und%20Ressourcen/24-03-27_Datenmodell_XFLB_Version_2.0.0_Ref.xlsx) nutzen.

## Aktuelle Informationen

- **Aktueller Stand und Entwicklungen**: In diesem Repository halten wir Sie über die neuesten Entwicklungen und Schritte des XFLB-Standards auf dem Laufenden.
- **Technische Spezifikationen und Dokumentationen**: Derzeit arbeiten wir daran, sämtliche relevanten Dokumentationen und technischen Spezifikationen hier bereitzustellen.

Erfahren Sie [hier](https://gitlab.opencode.de/OC000029002073/xflb/-/wikis/Startseite/2.-Aktuelles-&-Updates) mehr über die aktuellen Themen des Standards.

## Mitwirkende

Ein herzliches Dankeschön an alle, die bereits zur Entwicklung des XFLB-Standards beigetragen haben. Wir schätzen jede Form der Unterstützung und sind bestrebt, eine aktive und engagierte Community um dieses Projekt aufzubauen.

## Kontakt

Für weitere Informationen oder bei Fragen zum Beitrag zum XFLB-Projekt kontaktieren Sie uns bitte über xflb@stmd.bayern.de.

## Lizenz

Dieses Projekt steht unter der [MIT-Lizenz](https://gitlab.opencode.de/OC000029002073/xflb/-/wikis/Startseite/Lizenz). Durch Ihre Teilnahme erklären Sie sich mit den Bedingungen einverstanden.

## FAQ & Hilfe

## Wir freuen uns darauf, gemeinsam mit Ihnen den XFLB-Standard zu einem Eckpfeiler der digitalen Transformation im deutschen Förderwesen zu machen. Lassen Sie uns diesen Weg gemeinsam gehen.
